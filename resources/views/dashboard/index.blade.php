

@extends('layout.master')

@section('content')

    <div class="container-fluid">
        <div class="row">
{{--            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Upload document</h4>
                        </div>

                        <div class="modal-body">
                            <form action="/upload" method="get" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" name="title" class="form-control"/>
                                </div>
                                <div class="form-group">
                                    <label for="type">System</label>
                                    <select class="form-control" name="system">
                                        @foreach($systems as $system)
                                            <option value="{{ $system->id }}">{{ $system->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="doc_type">Document Type</label>
                                    <select class="form-control" name="doc_type">
                                        @foreach($document_list as $list)
                                            <option value="{{ $list->id }}">{{ $list->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <input type="hidden" class="" name="user" value="{{ Auth::getUser()->id }}"/>
                                <div class="form-group">
                                    <input type="file" class="" name="upload"/>
                                </div>

                                <div class="form-group">
                                    <textarea class="form-control" name="description" placeholder="Enter description"></textarea>
                                </div>
                                <hr>
                                <button type="submit" class="btn btn-primary" name="submit">Upload</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>--}}
            <div class="col-sm-12 {{--col-sm-offset-3 col-md-10 col-md-offset-2--}}">
                <div>
                    <h2 class="sub-header page-header">
                        <a class="btn btn-success" href="{{ url('/upload') }}"{{-- data-toggle="modal" data-target="#myModal"--}}>Upload Document</a></h2>

                </div>
                @if ($errors->has())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered table-hovered table-striped">
                        <thead>
                        <tr>
                            <th><a href="?sort=title&order={{ $sort != 'title' ? 'asc' : ( $order == 'asc' ? 'desc': 'asc')  }}">Title</a></th>
                            <th><a href="?sort=description&order=asc">Description</a></th>
                            <th>
                                <li class="dropdown" style=" list-style: none">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Document type<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/dashboard">All</a></li>
                                        @foreach($document_types as $document_type)
                                            <li><a href="/dashboard?document={{ $document_type->id }}" >{{ $document_type->title }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            </th>
                            <th>
                                <li class="dropdown" style="list-style: none">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Uploaded by<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="/dashboard">All</a></li>
                                        @foreach($users as $user)
                                            <li><a href="/dashboard?user={{ $user->id }}" >{{ $user->fullName() }}</a></li>
                                        @endforeach

                                    </ul>
                                </li>
                            </th>
                            <th><a href="?sort=created_at&order=asc">Date Uploaded</a></th>
                            {{--<td><a href="#"><span class="fui-eye"></span></a></td>--}}
                        </tr>
                        </thead>

                        <tbody>
                        @if($documents->count())
                            @foreach($documents as $document)
                                <tr>
                                    <td>{{ $document->title}}</td>
                                    <td>{{ $document->description }}</td>
                                    <td>{{ $document->type->title }}</td>
                                    <td>{{ $document->user->fullName() }}</td>
                                    <td>{{ $document->created_at->diffForHumans()}}</td>
                                    <td><a href="{{ $document->path }}" download="">Download</a></td>
                                    <td><a href="/delete/{{ $document->id }}"><span class="fui-trash"></span></a></td>
                                </tr>
                            @endforeach
                        @else
                            <td colspan="8"><div style="text-align: center; padding: 3em; font-size: 3em; color: #ccc;">
                                    <div><span></span></div>Nothing Found</div>
                            </td>
                        @endif
                        </tbody>

                    </table>
                </div>

                {{--<div>{!! $documents->render() !!}</div>--}}
            </div>
        </div>

@endsection





