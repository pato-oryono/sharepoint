@extends('layout.master')

@section('content')

    <div class="col-lg-12"><a data-toggle="tooltip" data-placement="top" title="Upload document" href="/upload"><span class="fui-plus-circle pull-right"></span></a></div>
    <div class="col-lg-12 col-md-12 col-sm-12">
    @if(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                    </div>
                    @endif
        @foreach($systems as $system)
                <div class="panel panel-default">
                    <div class="panel-heading">{{ $system->name }}</div>
                    <div class="panel-body">
                    <div>{{ $system->description }}</div><hr>
                        <div class="row">
                        <div class="col-lg-6">
                        @foreach($document_list as $list)
                            <div class="col-xs-6 col-sm-6 col-lg-2 card" style="text-align: center"><a href="document?system_id={{ $system->id }}&doc_id={{$list->id }}"><span
                                    @if($list->id == 1 )
                                        style="color: #43C6DB"
                                        class="fa fa-2x fa-server"
                                        data-toggle="tooltip" title="{{ $list->name }}"
                                    @elseif($list->id == 2)
                                        style="color: #C25283"
                                        class="fa fa-2x fa-area-chart"
                                        data-toggle="tooltip" title="{{ $list->name }}"
                                    @elseif($list->id == 3)
                                        style="color: #795548"
                                        class="fa fa-2x fa-book"
                                        data-toggle="tooltip" title="{{ $list->name }}"
                                    @elseif($list->id == 4)
                                        style="color: #727272"
                                        class="fa fa-2x fa-file-powerpoint-o"
                                        data-toggle="tooltip" title="{{ $list->name }}"
                                    @elseif($list->id == 5)
                                        style="color: #607D8B"
                                        class="fa fa-2x fa-archive"
                                        data-toggle="tooltip" title="{{ $list->name }}"
                                    @endif>
                                    </span>{{--{{ $list->name }}--}}</a></div>
                        @endforeach
                        </div>
                        <div class="col-lg-6"></div>

                        </div>
                    </div>
                </div>
        @endforeach
    </div>

@endsection


