@extends('layout.master')


@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                @if ($errors->has())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br>
                        @endforeach
                    </div>
                @elseif(Session::has('success'))
                    <div class="alert alert-success">
                        {{ Session::get('success') }}
                        <a href="/welcome">view documents</a>
                    </div>

                @endif
                <form action="/upload" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="title">Title <span style="color: red">*</span></label>
                        <input type="text" name="title" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="system">Document Type <span style="color: red">*</span></label>
                        <select class="form-control" name="document_type">
                            @foreach($document_types as $document_type)
                                <option value="{{ $document_type->id }}">{{ $document_type->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="hidden" class="" name="user" value="{{ Auth::getUser()->id }}"/>
                    <div class="form-group">
                        <input type="file" class="" name="upload"/>
                    </div>
                    <div class="form-group">
                        <label for="description">Description <span style="color: red">*</span></label>
                        <textarea class="form-control" name="description" placeholder="Enter description"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary" name="submit">Upload</button>
                    <a class="btn btn-danger" href="/welcome">Cancel</a>
                </form>
            </div>
            <div class="col-lg-6"></div>
        </div>
    </div>
@endsection



