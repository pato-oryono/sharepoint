<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
    <link rel="stylesheet" href="{{ asset('/assets/css/vendor/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/assets/css/flat-ui.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('/assets/css/flat-ui.min.css') }}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="">

<!-- Custom styles for this template -->
    <link href="{{asset('/assets/css/dashboard.css')}}" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

</head>
<body>
<div class="container-fluid">
    @include('partials.header')
    @yield('content')
</div>

<script src="{{ asset('/assets/js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('/assets/js/vendor/jquery-1.11.1.min.js') }}"></script>
<script src="{{ asset('/assets/js/flat-ui.min.js') }}"></script>
<script src="{{ asset('/assets/js/vendor/selectize.js') }}"></script>

</body>
</html>