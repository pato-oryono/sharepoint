<div class="navbar navbar-inverse {{--navbar-fixed-top--}}" role="navigation">
     <div class="container-fluid">
         <div class="navbar-header">
             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                 <span class="sr-only">Toggle navigation</span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
             </button>
             <a class="navbar-brand" href=""><span class="fui-document"></span>  Sharepoint</a>
         </div>
         <div class="navbar-collapse collapse">
             <ul class="nav navbar-nav navbar-right">
                 @if(Auth::guest())
                     <li><a href="{{ url('/') }}">Login</a></li>
                 @else
                     <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fui-user"></span> {{ Auth::getUser()->fullName() }} <b class="caret"></b></a>
                         <ul class="dropdown-menu">
                             @if(Auth::getUser()->type() == 'Admin')
                                 <li><a href="/welcome">Dashboard</a></li>
                                 <li><a href="/admin">Users</a></li>
                                 <li><a href="/doc-types">Document types</a></li>
                                 <li><a href="/documents">Documents</a></li>
                             @endif
                             <li><a href="/change_password">Change Password</a></li>
                             <li class="divider"></li>
                             <li><a href="{{ url('/') }}">Logout</a></li>
                         </ul>
                     </li>
                 @endif
             </ul>
             <form class="navbar-form navbar-right" action="/searches">
                 <div class="input-group">
                     <input class=" pull-right search-custom" id="searchbox" name="q" type="text" placeholder="Search"/>
                     {{--<div class="input-group-btn"><button class="btn btn-info search-btn-custom" type="submit"><span class="fui-search"></span></button></div>--}}
                 </div>
             </form>
         </div>
     </div>
 </div>


