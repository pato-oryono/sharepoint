<?php

use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $document_types = array(
            array(
                'title' => 'Deployment Architecture',
                'description' => '',
            ),
            array(
                'title' => 'Technical Architecture',
                'description' => '',
            ),
            array(
                'title' => 'Integration Arhitecture ',
                'description' => '',
            ),
            array(
                'title' => 'Service Catalogue',
                'description' => '',
            ),
            array(
                'title' => 'Capability Plan',
                'description' => '',
            )
        );
        foreach($document_types as $document_type){
            \App\DocumentType::create($document_type);
        }
    }
}
