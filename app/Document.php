<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['title', 'description', 'path', 'tag', 'tag', 'document_type', 'uploaded_by'];

    public function user()
    {
        return $this->belongsTo('App\User', 'uploaded_by');
    }

    public function type()
    {
        return $this->belongsTo('App\DocumentType','document_type');
    }

}
